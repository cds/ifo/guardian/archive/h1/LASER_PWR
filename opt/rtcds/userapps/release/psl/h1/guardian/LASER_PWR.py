import time
from guardian import GuardState, GuardStateDecorator
# These are located in userapps, /sys/common/guardian/
import ifolib.rs_power_control as rsp
import lscparams

##################################################

# reset power in Watts
RESET_POWER = 2

# Default power to go to after lockloss
DEFAULT_POWER = 2

# list of requestable powers in Watts
REQUEST_POWERS = [DEFAULT_POWER,10,25,35,50]

# list of fast powers, for initial alignment
fast_powers = []
for ifoconfig, power in lscparams.input_power.items():
    if (ifoconfig not in 'NLN') and (power != DEFAULT_POWER):
        # don't make a fast state for NLN's power
        if power not in fast_powers:
            fast_powers.append(power)




# Max power available on GRD
MAX_POWER = 100

# Default RS velocities
RS_VELOCITY = {'FAST':1500, 'SLOW':300}
# Default RS velocities RS_VELOCITY = {'FAST':3000, 'SLOW':300}
# The minimum velocity should only be 30, anything lower and the RS will get confused.
# 4-29-16 RS velocites have changed by a factor of 30. (ie. 10 is now 300)

#nominal = 'POWER_{}W'.format(REQUEST_POWERS[-3])
nominal = 'POWER_%sW'%lscparams.input_power['NLN']  # Changed to just copy the NLN power from params. 5June2019 JCD

##################################################
## Decorators adn Useful Functions

class check_error_bit(GuardStateDecorator):
    def pre_exec(self):
        # 0 = NoError string value not working
        if ezca['PSL-ROTATIONSTAGE_RS_ERROR'] != 0:
            notify('Error in Rotation Stage. Search for Home may fix.')
            ### Seems like this should also go to FAULT, but we need to test first.

class status_check(GuardStateDecorator):
    def pre_exec(self):
        if ezca['PSL-ROTATIONSTAGE_RS_INTERLOCK'] == 'Busy':
            notify('RS not enabled')
        # 0 = NoError
        if ezca['PSL-ROTATIONSTAGE_RS_MTR_ERROR'] != 0:
            notify('RS Motor error')
        if ezca['PSL-ROTATIONSTAGE_STATUS'] != 'Ok':
            notify('RS status is not Ok')
        if ezca['PSL-ROTATIONSTAGE_AUTOABORT'] != 1:
            notify('RS auto abort not on!')
##########

def collect_data(chan, duration):
    time_start = time.time()
    data = []
    while time.time() < time_start + duration:
        data.append(ezca[chan])
        time.sleep(0.2)
    return data

def get_acc_dec(velocity):
    """Assuming a linear relationship, calculate an acceleration
    and a deceleration time in ms.
    """
    return -20*velocity + 66000

##################################################

class INIT(GuardState):
    request = True
    @status_check
    def main(self):
        # Check for an error
        # Uset his or ERROR_FLAG?
        if ezca['PSL-ROTATIONSTAGE_RS_ERROR'] != 0:
            return 'FAULT'
        # See where the RS request is at
        RS_request_pwr = ezca['PSL-ROTATIONSTAGE_POWER_REQUEST']
        # Check the power in
        RS_pwr_in = ezca['PSL-ROTATIONSTAGE_POWER_IN']

        if RS_request_pwr == 0:
            return 'POWER_0W'
        elif (RS_request_pwr - RS_pwr_in) / RS_request_pwr <= 0.15:
            return 'POWER_{}W'.format(int(RS_request_pwr))
        elif (RS_request_pwr - RS_pwr_in) / RS_request_pwr > 0.15:
            notify('Requested power and power in are not close, search for home.')
    @status_check
    def run(self):
        # See what the RS thinks it is at
        RS_request_pwr = ezca['PSL-ROTATIONSTAGE_POWER_REQUEST']
        # Check the power in
        RS_pwr_in = ezca['PSL-ROTATIONSTAGE_POWER_IN']
        notify_flag = False

        if (RS_request_pwr - RS_pwr_in) / RS_request_pwr <= 0.15:
            return 'POWER_{}W'.format(int(RS_request_pwr))
        elif ((RS_request_pwr - RS_pwr_in) / RS_request_pwr > 0.15) and not notify_flag:
            notify('Requested power and power in are not close, search for home.')
            notify('Or adjust the RS by hand to the desired power')
            # This is to keep it from repeating in log
            notify_flag = True
            return False


def gen_goto_state(power, index, rs_velocity=RS_VELOCITY['SLOW']):
    class POWER(GuardState):
        goto = True
        request = False
        redirect = False
        @check_error_bit
        @status_check
        def main(self):
            self.starting_power = ezca['IMC-PWR_IN_OUTMON']
            self.pwr_diff = abs(int(self.starting_power) - power)

            ezca['PSL-ROTATIONSTAGE_RS_VELOCITY'] = rs_velocity
            ezca['PSL-ROTATIONSTAGE_RS_ACCELERATION'] = get_acc_dec(rs_velocity)
            ezca['PSL-ROTATIONSTAGE_RS_DECELERATION'] = get_acc_dec(rs_velocity)
            # Make sure that the fine adjust is off
            ezca['PSL-ROTATIONSTAGE_FINEADJUST'] = 0

            log("Moving IFO input power to %s watts..." % power)
            ezca['PSL-ROTATIONSTAGE_POWER_REQUEST'] = power
            # Needs this otherwise the button is pressed before the power is in
            time.sleep(0.1)
            # Press the 'GO_TO_POWER' button
            ezca['PSL-ROTATIONSTAGE_COMMAND'] = 2
            self.timer['wait_for_busy'] = 1
            self.timer['pause_between_cmds'] = 0
            self.bootstraped = False
            self.initial_done = False

        @check_error_bit
        @status_check
        def run(self):
            # Don't do bootstrapping if Can't read power. JCD 4Sept2020
            if ezca['PSL-PWR_PMC_TRANS_OUTPUT'] < 1.0:
                log('PMC not locked')
                return 'FAULT'

            if not self.timer['wait_for_busy']:
                return False
            # Adjust the power scaling while the RS is moving and make sure the PMC is locked
            # Using the 'Busy' did not work, Busy = 1 Idle = 0
            elif self.timer['wait_for_busy'] and ezca['PSL-ROTATIONSTAGE_STATE_BUSY'] == 1 and ezca['PSL-PMC_LOCK_ON'] == -30000:
                # Set the offset to the current power
                ezca['PSL-POWER_SCALE_OFFSET'] = '{:.4}'.format(ezca['IMC-PWR_IN_OUTMON'])
            # After we done with the initial move, and now to bootstrap
            elif self.timer['wait_for_busy'] and ezca['PSL-ROTATIONSTAGE_STATE_BUSY'] == 0 and ezca['PSL-PMC_LOCK_ON'] == -30000:
                if self.bootstraped:
                    return True
                elif not self.initial_done:
                    self.timer['pause_between_cmds'] = 2
                    self.initial_done = True
                elif self.timer['pause_between_cmds'] and self.initial_done:
                    log('RS done at power: {}. Bootstrapping.'.format(ezca['IMC-PWR_IN_OUTMON']))
                    rsp.rs_bootstrap_power('PSL', power) # Commented out PJT
                    self.bootstraped = True
                    # FIXME: Not sure if we need this here, need to test
                    self.timer['wait_for_busy'] = 1
    POWER.index = index
    return POWER


def gen_idle_state(index, requestable=False):
    class IDLE_STATE(GuardState):
        request = requestable
        @check_error_bit
        @status_check
        def main(self):
            # This acts as a double check incase the it was missed somehow
            ezca['PSL-POWER_SCALE_OFFSET'] = '{:.4}'.format(ezca['IMC-PWR_IN_OUTMON'])
        @check_error_bit
        @status_check
        def run(self):
            #  Removed 22June2016, JCD, SED, KI, to not break ISS 3rd loop
            #if abs(ezca['PSL-POWER_SCALE_OFFSET'] - ezca['IMC-PWR_IN_OUTMON'])> 0.5:
            #    ezca['PSL-POWER_SCALE_OFFSET'] = ezca['IMC-PWR_IN_OUTMON']
            if ezca['IMC-PWR_IN_OUTMON'] < 0.3:
                log('IMC has no laser power!')
                return 'FAULT'
            elif (abs(ezca['PSL-ROTATIONSTAGE_POWER_REQUEST'] - ezca['IMC-PWR_IN_OUTMON']) / ezca['IMC-PWR_IN_OUTMON']) >= 0.20:
                notify('Power not the same as requested')
            else:
                return True
    IDLE_STATE.index = index
    return IDLE_STATE


class RESETTING_POWER(GuardState):
    index = 1
    goto = True
    request = False
    @check_error_bit
    @status_check
    def main(self):
        log("Moving PSL rotation stage to {} watt...".format(RESET_POWER))
        # Set PSL-ROTATIONSTAGE_POWER_IN = IMC-PWR_EOM_OUTPUT
        rsp.rs_update_calc('PSL')
        # Bring it back to 2W
        ezca['PSL-ROTATIONSTAGE_RS_VELOCITY'] = RS_VELOCITY['FAST']
        ezca['PSL-ROTATIONSTAGE_RS_ACCELERATION'] = get_acc_dec(RS_VELOCITY['FAST'])
        ezca['PSL-ROTATIONSTAGE_RS_DECELERATION'] = get_acc_dec(RS_VELOCITY['FAST'])
        # This doesn't work if the request is already that
        log("Moving IFO input power to {} watts...".format(RESET_POWER))
        ezca['PSL-ROTATIONSTAGE_POWER_REQUEST'] = RESET_POWER
        # Needs this otherwise the button is pressed before the power is in
        time.sleep(0.1)
        # Press the 'GO_TO_POWER' button
        ezca['PSL-ROTATIONSTAGE_COMMAND'] = 2
        time.sleep(1)
    @check_error_bit
    @status_check
    def run(self):
        if ezca['PSL-ROTATIONSTAGE_STATE_BUSY']:
            # Set the offset to the current power
            ezca['PSL-POWER_SCALE_OFFSET'] = ezca['IMC-PWR_IN_OUTMON']
        else:
            return True

POWER_RESET = gen_idle_state(2,requestable=True)


class SEARCHING_FOR_HOME(GuardState):
    """Hit the command through the epics channel and move to home

    """
    index = 4
    request = False
    goto = True
    @status_check
    def main(self):
        log('Rot. stage moving to Home')
        # Give the search for home command
        ezca['PSL-ROTATIONSTAGE_COMMAND'] = 1
        time.sleep(1)
        ezca['PSL-POWER_SCALE_OFFSET'] = ezca['IMC-PWR_IN_OUTMON']
        # Needs a small break otherwise it jumps ahead
        self.timer['searching'] = 8
        time.sleep(0.5)
    @status_check
    def run(self):
        if ezca['PSL-ROTATIONSTAGE_STATE_BUSY']:
            # Set the offset to the current power
            ezca['PSL-POWER_SCALE_OFFSET'] = ezca['IMC-PWR_IN_OUTMON']
        # Use the timer to make sure that the node didn't run through this state.
        # It seems like the RS can take a bit of time before it actually executes the command
        elif self.timer['searching']:
            return True


class HOME(GuardState):
    """Idle state to wait at home until a power is requested and not check
    that the poweer request is near the IMC output power (like gen_idle_state)
    """
    index = 5
    goto = False
    @status_check
    def main(self):
        return True
    @status_check
    def run(self):
        return True


class FAULT(GuardState):
    """Turn off the autolockers if the laser is no longer lasing.
    Test to see if power has returned.
    TODO: Find what the error codes mean and report them.
    """
    redirect = False
    def main(self):
        if ezca['PSL-LASER_AMP2_PWR'] < 1:
            # Turn off the auto lockers so the pzts are working while there is no light
            ezca['PSL-FSS_AUTOLOCK_ON'] = 0
            ezca['PSL-PMC_LOCK_ON'] = 0
            ezca['PSL-PMC_RAMP_ON'] = 0
            ezca['PSL-ISS_AUTOLOCK_ON'] = 0
            ezca['PSL-ISS_LOOP_STATE_REQUEST'] = 0
            ezca['PSL-ISS_SECONDLOOP_OUTPUT_SWITCH'] = 0
    @check_error_bit
    @status_check
    def run(self):
        if ezca['IMC-PWR_IN_OUTMON'] < 0.5:
            notify('IMC has no power!')
            return False
        elif ezca['PSL-PWR_PMC_TRANS_OUTPUT'] < 1.0:
            notify('PMC probably not locked')
            return False
        else:
            return True

##################################################
edges = []

edges += [('SEARCHING_FOR_HOME', 'HOME'),
          ('RESETTING_POWER','POWER_RESET')]

# Slow to max requestable power
#globals()['GOTO_POWER_{}W_SLOW'.format(REQUEST_POWERS[-1])] = gen_goto_state(REQUEST_POWERS[-1], REQUEST_POWERS[-1] * 10 - 3, rs_velocity=RS_VELOCITY['SLOW'])
# The Max PWR state will be generated

# Fast back to default power
globals()['GOTO_POWER_{}W_FAST'.format(DEFAULT_POWER)] = gen_goto_state(DEFAULT_POWER, DEFAULT_POWER * 10 - 6, rs_velocity=RS_VELOCITY['FAST'])
# A 2W state will already be generated

# Fast to init alignment powers (state numbers should be 'tuned' to make more sense)
for ii in range(len(fast_powers)):
    state_goto = 'GOTO_POWER_{}W_FAST'.format(fast_powers[ii])
    state_extraIdle = 'POWER_{}W_IDLE'.format(fast_powers[ii])
    state_regularIdle = 'POWER_{}W'.format(fast_powers[ii])
    globals()[state_goto]      = gen_goto_state(fast_powers[ii], fast_powers[ii] * (10000) - 6, rs_velocity=RS_VELOCITY['FAST'])
    globals()[state_extraIdle] = gen_idle_state(fast_powers[ii] * (1000) - 6,) # extra state to make this path longer
    edges += [(state_goto, state_extraIdle)]
    edges += [(state_extraIdle, state_regularIdle)]


##################################################
# automatically generate basic power request states
# This should make the powers in the request list, requestable
# and then every integer power should be available as non-requestable
for i in range(0, MAX_POWER+1):
    index = i * 10
    state_goto = 'GOTO_POWER_{}W'.format(i)
    state_idle = 'POWER_{}W'.format(i)
    # Dont make a goto for the fast/slows
    if i not in [DEFAULT_POWER]:
        if i == 0:
            globals()[state_goto] = gen_goto_state(i, 6)
        else:
            globals()[state_goto] = gen_goto_state(i, index-1)
    # Make requestable
    if i in REQUEST_POWERS:
        globals()[state_idle] = gen_idle_state(index, requestable=True)
    elif i == 0:
        globals()[state_idle] = gen_idle_state(7)
    else:
        globals()[state_idle] = gen_idle_state(index)
    # These edges are already made
    if i not in [DEFAULT_POWER]:
        edges += [(state_goto, state_idle)]


##################################################
#edges += [('GOTO_POWER_{}W_SLOW'.format(REQUEST_POWERS[-1]), 'POWER_{}W'.format(REQUEST_POWERS[-1]))]
edges += [('GOTO_POWER_{}W_FAST'.format(DEFAULT_POWER), 'POWER_{}W'.format(DEFAULT_POWER))]

##################################################
# SVN $Id$
# $HeadURL$
##################################################
